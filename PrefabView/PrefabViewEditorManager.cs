﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BicUtil.PrefabView{
	public class PrefabViewEditorManager : MonoBehaviour {
		public Transform PrefabLayer;
		public string LastScenePath;
		public PrefabView SelectedPrefabView;

	}
}