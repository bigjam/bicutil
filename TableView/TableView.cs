﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SocialPlatforms;
using BicDB.Container;
using BicDB;
using System;
using BicUtil.Tween;

namespace BicUtil.TableView
{
    /// <summary>
    /// A reusable table for for (vertical) tables. API inspired by Cocoa's UITableView
    /// Hierarchy structure should be :
    /// GameObject + TableView (this) + Mask + Scroll Rect (point to child)
    /// - Child GameObject + Vertical Layout Group
    /// This class should be after Unity's internal UI components in the Script Execution Order
    /// </summary>
	[RequireComponent(typeof(EventControlledScrollRect))]
    public class TableView : MonoBehaviour
    {

        #region Public API
        /// <summary>
        /// The data source that will feed this table view with information. Required.
        /// </summary>

		public void SetDBSource<T>(IList<T> _table, Func<TableView, IList<T>, int, float> _getRowHeightFunc = null) where T : IRecordContainer, new(){
			GetCellDataFunc = (int _cellIndex) => {
                if(_table.Count <= _cellIndex){
                    return null;
                }

				return _table[_cellIndex];	
			};

			GetTableSizeFunc = () => {
				return _table.Count;
			};

			DataSource = new TableDataSourceAuto<T>(_table, _getRowHeightFunc);
		}

        [System.Serializable]
        public class RowVisibilityChangeEvent : UnityEvent<int, bool> { }
        /// <summary>
        /// This event will be called when a cell's visibility changes
        /// First param (int) is the row index, second param (bool) is whether or not it is visible
        /// </summary>
        public RowVisibilityChangeEvent onRowVisibilityChanged;

        /// <summary>
        /// Get a cell that is no longer in use for reusing
        /// </summary>
        /// <param name="reuseIdentifier">The identifier for the cell type</param>
        /// <returns>A prepared cell if available, null if none</returns>
        public TableRow GetReusableRow(string reuseIdentifier) {
			
            LinkedList<TableRow> _rows;
            if (!m_reusableRows.TryGetValue(reuseIdentifier, out _rows)) {
                return null;
            }

            if (_rows.Count == 0) {
                return null;
            }

            TableRow _row = _rows.First.Value;
            _rows.RemoveFirst();
            return _row;
        }
        
        private Dictionary<string, Action<IRecordContainer>> onClickedCellActions = new Dictionary<string, Action<IRecordContainer>>();
        public void AddClickCellEvent(string _buttonName, Action<IRecordContainer> _action){
            onClickedCellActions[_buttonName] = _action;
        }

		public TableRow CreateTableRow(){
			TableRow _row = GetReusableRow(tableRow.reuseIdentifier);

			if (_row == null) {
				_row = (TableRow)GameObject.Instantiate(tableRow);
                _row.BindOnClickedEvent(onClickedCellActions);
				_row.name = "RowInstance";
				_row.gameObject.SetActive(true);
			}

			return _row;
		}

		public int GetTableSize(){
			if (GetTableSizeFunc == null) {
				return 0;
			}

			return GetTableSizeFunc();
		}

		public float GetRowHeight(){
			return m_rowHeight;
		}

        public void StopScrollMovement(){
            m_scrollRect.StopMovement();
        }

        public bool isEmpty { get; private set; }

        /// <summary>
        /// Reload the table view. Manually call this if the data source changed in a way that alters the basic layout
        /// (number of rows changed, etc)
        /// </summary>
        public void ReloadData() {
            m_rowSizes = new float[(int)Math.Ceiling((float)m_dataSource.GetNumberOfCellsForTableView(this) / (float)cellCountInARow)];
            this.isEmpty = m_rowSizes.Length == 0;

            if (this.isEmpty) {
                ClearAllRows();
                return;
            }
            m_cumulativeRowSizes = new float[m_rowSizes.Length];
            m_cleanCumulativeIndex = -1;

            for (int i = 0; i < m_rowSizes.Length; i++) {
                m_rowSizes[i] = m_dataSource.GetHeightForRowInTableView(this, i * cellCountInARow);
                if (i > 0) {
                    m_rowSizes[i] += m_LayoutGroup.spacing;
                }
            }


			if(m_isVertical) {
				m_scrollRect.content.sizeDelta = new Vector2(m_scrollRect.content.sizeDelta.x, 
					GetCumulativeRowHeight(m_rowSizes.Length - 1) + m_LayoutGroup.padding.top + m_LayoutGroup.padding.bottom);
			} else {
				m_scrollRect.content.sizeDelta = new Vector2(GetCumulativeRowHeight(m_rowSizes.Length - 1) + m_LayoutGroup.padding.left + m_LayoutGroup.padding.right, m_scrollRect.content.sizeDelta.y);
			}

            RecalculateVisibleRowsFromScratch();
            m_requiresReload = false;
            // scrollDistance = 1;
            // scrollDistance = 0; 

        }

        /// <summary>
        /// Get row at a specific row (if active). Returns null if not.
        /// </summary>
        public TableRow GetRow(int _rowIndex)
        {
            TableRow _result = null;
            m_visibleRows.TryGetValue(_rowIndex, out _result);
            return _result;
        }

        /// <summary>
        /// Get the range of the currently visible rows
        /// </summary>
        public Range visibleRowRange {
            get { return m_visibleRowRange; }
        }

        /// <summary>
        /// Notify the table view that one of its rows changed size
        /// </summary>
        public void NotifyRowDimensionsChanged(int _rowIndex) {
            float oldHeight = m_rowSizes[_rowIndex];
            m_rowSizes[_rowIndex] = m_dataSource.GetHeightForRowInTableView(this, _rowIndex);
            m_cleanCumulativeIndex = Mathf.Min(m_cleanCumulativeIndex, _rowIndex - 1);
            if (m_visibleRowRange.Contains(_rowIndex)) {
                TableRow _row = GetRow(_rowIndex);

				if(m_isVertical) {
					_row.GetComponent<LayoutElement>().preferredHeight = m_rowSizes[_rowIndex];
					if(_rowIndex > 0) {
						_row.GetComponent<LayoutElement>().preferredHeight -= m_LayoutGroup.spacing;
					}
				} else {
					_row.GetComponent<LayoutElement>().preferredWidth = m_rowSizes[_rowIndex];
					if(_rowIndex > 0) {
						_row.GetComponent<LayoutElement>().preferredWidth -= m_LayoutGroup.spacing;
					}
				}
            }

            float heightDelta = m_rowSizes[_rowIndex] - oldHeight;
            
			if(m_isVertical) {
				m_scrollRect.content.sizeDelta = new Vector2(m_scrollRect.content.sizeDelta.x,
					m_scrollRect.content.sizeDelta.y + heightDelta);
			} else {
				m_scrollRect.content.sizeDelta = new Vector2(m_scrollRect.content.sizeDelta.x + heightDelta, m_scrollRect.content.sizeDelta.y);
			}

            m_requiresRefresh = true;
        }

        /// <summary>
        /// Get the maximum scrollable height of the table. scrollY property will never be more than this.
        /// </summary>
        public float scrollableDistance {
            get {
				if(m_isVertical) {
					return m_scrollRect.content.rect.height - (this.transform as RectTransform).rect.height;
				} else {
					return m_scrollRect.content.rect.width - (this.transform as RectTransform).rect.width;
				}
            }
        }

        /// <summary>
        /// Get or set the current scrolling position of the table
        /// </summary>
        public float scrollDistance {
            get {
				return m_scrollDistance;
            }
            set {
                if (this.isEmpty) {
                    return;
                }
                value = Mathf.Clamp(value, 0, GetScrollYForRow(m_rowSizes.Length - 1, true));
                if (m_scrollDistance != value) {
                    m_scrollDistance = value;
                    m_requiresRefresh = true;

					float relativeScroll = value / this.scrollableDistance;

					if(m_isVertical) {
						m_scrollRect.verticalNormalizedPosition = 1 - relativeScroll;
					} else {
						m_scrollRect.horizontalNormalizedPosition = relativeScroll;
					}
                }
            }
        }

        /// <summary>
        /// Get the y that the table would need to scroll to to have a certain row at the top
        /// </summary>
        /// <param name="_rowIndex">The desired row</param>
        /// <param name="_above">Should the top of the table be above the row or below the row?</param>
        /// <returns>The y position to scroll to, can be used with scrollY property</returns>
        public float GetScrollYForRow(int _rowIndex, bool _above) {
            float retVal = GetCumulativeRowHeight(_rowIndex);
            if (_above) {
                retVal -= m_rowSizes[_rowIndex];
            }
            return retVal;
        }

        #endregion

        #region Private implementation

		[SerializeField]
		private TableRow tableRow;
        private int cellCountInARow = 0;

        private ITableViewDataSource m_dataSource;
        private bool m_requiresReload;

		private HorizontalOrVerticalLayoutGroup m_LayoutGroup;
		private EventControlledScrollRect m_scrollRect;
        private LayoutElement m_topPadding;
        private LayoutElement m_bottomPadding;

		private float[] m_rowSizes;
        //cumulative[i] = sum(rowHeights[j] for 0 <= j <= i)
		private float[] m_cumulativeRowSizes;
        private int m_cleanCumulativeIndex;

        private Dictionary<int, TableRow> m_visibleRows;
		private Range m_visibleRowRange;
        public Range VisibleRowRange{
            get{ return m_visibleRowRange;}
        }

        private RectTransform m_reusableRowContainer;
        private Dictionary<string, LinkedList<TableRow>> m_reusableRows;

        private float m_scrollDistance;
        private bool m_requiresRefresh;

		private bool m_isVertical;
		private float m_rowHeight;

		public ITableViewDataSource DataSource
		{
			get { return m_dataSource; }
			set { m_dataSource = value; m_requiresReload = true; }
		}

		private Func<int, IRecordContainer> GetCellDataFunc = null;
		private Func<int> GetTableSizeFunc = null;

        private void ScrollViewValueChanged(Vector2 newScrollValue) {
			if(m_isVertical) {
				float relativeScroll = 1 - newScrollValue.y;
				m_scrollDistance = relativeScroll * scrollableDistance;
			} else {
				float relativeScroll = newScrollValue.x;
				m_scrollDistance = relativeScroll * scrollableDistance;
			}

            m_requiresRefresh = true;
        }

        private void RecalculateVisibleRowsFromScratch() {
            ClearAllRows();
            SetInitialVisibleRows();
        }

        private void ClearAllRows() {
            while (m_visibleRows.Count > 0) {
                HideRow(false);
            }
            m_visibleRowRange = new Range(0, 0);
        }

        void Awake()
        {
			m_isVertical = true;
            isEmpty = true;
			m_scrollRect = GetComponent<EventControlledScrollRect>();
			m_LayoutGroup = m_scrollRect.content.GetComponentInChildren<VerticalLayoutGroup>();
            
			if(m_LayoutGroup == null) {
				m_LayoutGroup = m_scrollRect.content.GetComponentInChildren<HorizontalLayoutGroup>();
				m_isVertical = false;
			}

			if(m_LayoutGroup == null) {
				throw new System.Exception("m_verticalLayoutGroup is null");
			}

			m_rowHeight = 100;
			
            if (tableRow != null) {
				tableRow.gameObject.SetActive(false);

				if (m_isVertical) {
					m_rowHeight = tableRow.GetComponent<RectTransform>().sizeDelta.y;
				} else {
					m_rowHeight = tableRow.GetComponent<RectTransform>().sizeDelta.x;
				}
			}
            
            cellCountInARow = tableRow.GetCellCount();

            m_topPadding = CreateEmptyPaddingElement("TopPadding");
            m_topPadding.transform.SetParent(m_scrollRect.content, false);
            m_bottomPadding = CreateEmptyPaddingElement("Bottom");
            m_bottomPadding.transform.SetParent(m_scrollRect.content, false);
            m_visibleRows = new Dictionary<int, TableRow>();

            m_reusableRowContainer = new GameObject("ReusableRows", typeof(RectTransform)).GetComponent<RectTransform>();
            m_reusableRowContainer.SetParent(this.transform, false);
            m_reusableRowContainer.gameObject.SetActive(false);
            m_reusableRows = new Dictionary<string, LinkedList<TableRow>>();
        }
        
        void Update()
        {
            if (m_requiresReload) {
                ReloadData();
            }
        }

        void LateUpdate() {
            if (m_requiresRefresh) {
                RefreshVisibleRows();
            }

			if (centerPositionMagnet == true) {
				checkEnableMagnet ();
			}
        }

        void OnEnable() {
            m_scrollRect.onValueChanged.AddListener(ScrollViewValueChanged);
			m_scrollRect.OnBeginDragActions.AddListener (isControlledTrue);
			m_scrollRect.OnEndDragActions.AddListener (isControlledFalse);
        }

        void OnDisable() {
			m_scrollRect.onValueChanged.RemoveListener(ScrollViewValueChanged);
			m_scrollRect.OnBeginDragActions.RemoveListener (isControlledTrue);
			m_scrollRect.OnEndDragActions.RemoveListener (isControlledFalse);
        }

		bool isControlled = false;
		private void isControlledTrue(){
			isControlled = true;
            scrollTweenCancelObject.Cancel();
		}

		private void isControlledFalse(){
			isControlled = false;
		}


		#region MangetControl
		[SerializeField]
		private bool centerPositionMagnet = false;
		float lastScrollDistance = 0;

		private void checkEnableMagnet(){
			if (isControlled == false) {
				if (scrollDistance >= 0 && scrollDistance <= scrollableDistance) {
					float _gap = lastScrollDistance - scrollDistance;
					if (Mathf.Abs (_gap) < 4f) {
						magnetControl (_gap);
						isControlled = true;
					}
				}
			}

			lastScrollDistance = scrollDistance;
		}

        private TweenCancelObject scrollTweenCancelObject = new TweenCancelObject();
		private void magnetControl(float _gap){
             if(m_isVertical == true){
                    throw new System.NotImplementedException("not support magnet control for vertical table");
             }

			var _centerPosition = m_scrollRect.transform.position;
			foreach (var _row in m_visibleRows) {
				var _rowPosition = _row.Value.transform.position;
				var _rowHalfSize = m_rowSizes [_row.Key] / 2f;
                if (_rowPosition.x + _rowHalfSize > _centerPosition.x && _rowPosition.x - _rowHalfSize <= _centerPosition.x) {
                    var _targetPosition = scrollDistance - (_centerPosition.x - _rowPosition.x);
                    float _targetGap = _targetPosition - scrollDistance;
                    var _speed = Mathf.Abs(_targetGap) / 100f;
                    BicTween.Value (scrollDistance, _targetPosition, _speed).SetEase(EaseType.OutBack).SubscribeUpdate(_value => {
                        scrollDistance = _value.x;
                    }).SetCancelObject(scrollTweenCancelObject);
                    break;
                }
			}
		}
		#endregion
        
        private Range CalculateCurrentVisibleRowRange()
        {
            float startY = 0;
			float endY = 0;
            
			if (m_isVertical) {
				startY = m_scrollDistance - m_LayoutGroup.padding.top;
				endY = m_scrollDistance + (this.transform as RectTransform).rect.height + m_LayoutGroup.padding.bottom;
			}else if(!m_isVertical) {
				startY = m_scrollDistance - m_LayoutGroup.padding.left;
				endY = m_scrollDistance + (this.transform as RectTransform).rect.width + m_LayoutGroup.padding.right;
			}

			int startIndex = FindIndexOfRowAtY(startY);
            int endIndex = FindIndexOfRowAtY(endY);
            return new Range(startIndex, endIndex - startIndex + 1);
        }

        private void SetInitialVisibleRows()
        {
            Range visibleRows = CalculateCurrentVisibleRowRange();

            for (int i = 0; i < visibleRows.count; i++)
            {   
                AddRow(visibleRows.from + i, true, m_scrollRect.content);
            }
            m_visibleRowRange = visibleRows;
            UpdatePaddingElements();
        }

        private void AddRow(int _rowIndex, bool _isEnd, RectTransform _parent)
        {
            TableRow newRow = m_dataSource.GetCellForRowInTableView(this, _rowIndex);

            newRow.SetData(_rowIndex * cellCountInARow, GetCellDataFunc);

            newRow.transform.SetParent(_parent, false);

			if(m_isVertical) {
				newRow.GetComponent<LayoutElement>().preferredHeight = m_rowSizes[_rowIndex];
				if(_rowIndex > 0) {
					newRow.GetComponent<LayoutElement>().preferredHeight -= m_LayoutGroup.spacing;
				}
			} else {
				newRow.GetComponent<LayoutElement>().preferredWidth = m_rowSizes[_rowIndex];
				if(_rowIndex > 0) {
					newRow.GetComponent<LayoutElement>().preferredWidth -= m_LayoutGroup.spacing;
				}
			}
            
            m_visibleRows[_rowIndex] = newRow;

            if (_isEnd) {
                newRow.transform.SetSiblingIndex(m_scrollRect.content.childCount - 2); //One before bottom padding
            } else {
                newRow.transform.SetSiblingIndex(1); //One after the top padding
            }

			this.onRowVisibilityChanged.Invoke(_rowIndex, true);

        }

        private void RefreshVisibleRows()
        {
            m_requiresRefresh = false;

            if (this.isEmpty) {
                return;
            }

            Range newVisibleRows = CalculateCurrentVisibleRowRange();

			int oldTo = m_visibleRowRange.Last();
            int newTo = newVisibleRows.Last();

            if (newVisibleRows.from > oldTo || newTo < m_visibleRowRange.from) {
                //We jumped to a completely different segment this frame, destroy all and recreate
				RecalculateVisibleRowsFromScratch();
                return;
            }

            //Remove rows that disappeared to the top
            for (int i = m_visibleRowRange.from; i < newVisibleRows.from; i++)
            {
                HideRow(false);
            }
            //Remove rows that disappeared to the bottom
            for (int i = newTo; i < oldTo; i++)
            {
                HideRow(true);
            }
            //Add rows that appeared on top
            for (int i = m_visibleRowRange.from - 1; i >= newVisibleRows.from; i--) {
                AddRow(i, false, m_scrollRect.content);
            }
            //Add rows that appeared on bottom
            for (int i = oldTo + 1; i <= newTo; i++) {
                AddRow(i, true, m_scrollRect.content);
            }
            m_visibleRowRange = newVisibleRows;
            UpdatePaddingElements();
        }

        private void UpdatePaddingElements() {
            float hiddenElementsHeightSum = 0;
            for (int i = 0; i < m_visibleRowRange.from; i++) {
                hiddenElementsHeightSum += m_rowSizes[i];
            }

			if(m_isVertical) {
				m_topPadding.preferredHeight = hiddenElementsHeightSum - m_LayoutGroup.padding.top;
				m_topPadding.gameObject.SetActive(m_topPadding.preferredHeight > 0);
			} else {
				m_topPadding.preferredWidth = hiddenElementsHeightSum - m_LayoutGroup.padding.left;
				m_topPadding.gameObject.SetActive(m_topPadding.preferredWidth > 0);
			}


			for (int i = m_visibleRowRange.from; i <= m_visibleRowRange.Last(); i++) {
                hiddenElementsHeightSum += m_rowSizes[i];
            }

			if(m_isVertical) {
				float bottomPaddingHeight = m_scrollRect.content.rect.height - hiddenElementsHeightSum;
				m_bottomPadding.preferredHeight = bottomPaddingHeight - m_LayoutGroup.spacing - m_LayoutGroup.padding.top - m_LayoutGroup.padding.bottom;
				m_bottomPadding.gameObject.SetActive(m_bottomPadding.preferredHeight > 0);
			} else {
				float bottomPaddingHeight = m_scrollRect.content.rect.width - hiddenElementsHeightSum;
				m_bottomPadding.preferredWidth = bottomPaddingHeight - m_LayoutGroup.spacing - m_LayoutGroup.padding.left- m_LayoutGroup.padding.right;
				m_bottomPadding.gameObject.SetActive(m_bottomPadding.preferredWidth > 0);
			}
        }

        private void HideRow(bool _last)
        {
            int _rowIndex = _last ? m_visibleRowRange.Last() : m_visibleRowRange.from;
            TableRow removedRow = m_visibleRows[_rowIndex];
			removedRow.OnRemove();
            StoreRowForReuse(removedRow);
            m_visibleRows.Remove(_rowIndex);
            m_visibleRowRange.count -= 1;
            if (!_last) {
                m_visibleRowRange.from += 1;
            } 
            this.onRowVisibilityChanged.Invoke(_rowIndex, false);
        }

        private LayoutElement CreateEmptyPaddingElement(string _name)
        {
            GameObject go = new GameObject(_name, typeof(RectTransform), typeof(LayoutElement));
            LayoutElement le = go.GetComponent<LayoutElement>();
            return le;
        }

        private int FindIndexOfRowAtY(float y) {
            //TODO : Binary search if inside clean cumulative row height area, else walk until found.
            return FindIndexOfRowAtY(y, 0, m_cumulativeRowSizes.Length - 1);
        }

        private int FindIndexOfRowAtY(float y, int startIndex, int endIndex) {
			if (startIndex >= endIndex) {
                return startIndex;
            }
            int midIndex = (startIndex + endIndex) / 2;

            if (GetCumulativeRowHeight(midIndex) >= y) {
                return FindIndexOfRowAtY(y, startIndex, midIndex);
            } else {
                return FindIndexOfRowAtY(y, midIndex + 1, endIndex);
            }
        }

        private float GetCumulativeRowHeight(int _rowIndex) {
            while (m_cleanCumulativeIndex < _rowIndex) {
                m_cleanCumulativeIndex++;
				m_cumulativeRowSizes[m_cleanCumulativeIndex] = m_rowSizes[m_cleanCumulativeIndex];
				 if (m_cleanCumulativeIndex > 0) {
                    m_cumulativeRowSizes[m_cleanCumulativeIndex] += m_cumulativeRowSizes[m_cleanCumulativeIndex - 1];
                } 
            }
            return m_cumulativeRowSizes[_rowIndex];
        }

        private void StoreRowForReuse(TableRow _row) {
            string reuseIdentifier = _row.reuseIdentifier;
            
            if (string.IsNullOrEmpty(reuseIdentifier)) {
                GameObject.Destroy(_row.gameObject);
                return;
            }

            if (!m_reusableRows.ContainsKey(reuseIdentifier)) {
                m_reusableRows.Add(reuseIdentifier, new LinkedList<TableRow>());
            }

            m_reusableRows[reuseIdentifier].AddLast(_row);
            _row.transform.SetParent(m_reusableRowContainer, false);
        }

        #endregion


        
    }

    internal static class RangeExtensions
    {
        public static int Last(this Range range)
        {
            if (range.count == 0)
            {
                throw new System.InvalidOperationException("Empty range has no to()");
            }
            return (range.from + range.count - 1);
        }

        public static bool Contains(this Range range, int num) {
            return num >= range.from && num < (range.from + range.count);
        }
    }

}
